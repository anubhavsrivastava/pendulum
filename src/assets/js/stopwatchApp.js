angular.module('PendulumApp',[])
    .controller('StopWatchController', function($scope) {
        var self = $scope;
        var Stopwatch = Pendulum.Stopwatch;
        self.stopWatches = [];
        self.messages = Stopwatch._id;
        self.stopWatches.push(Object.create(Stopwatch)); 

        self.addWatch = function() {
        	if(self.stopWatches.length>7)
        	{  var msg = "Currently Pendulum Supports upto 8 Stopwatches at once."
        		Materialize?Materialize.toast(msg, 4000):  alert(msg);
        		return;
        	}
        	$('.tabwatch').addClass('hide');
            self.stopWatches.push(Object.create(Stopwatch));
            setTimeout(function(){  
            	$('ul.tabs').tabs();$('#'+(self.stopWatches.length-1)).click();
            	$('.tabwatch').removeClass('hide');
        }, 10);
           
             
            
        };

        self.startWatch = function(index) {
            self.stopWatches[index].initialize();
            self.stopWatches[index].addEventListener(function(data) {
                var html = self.getDateHtml(data);
                $('#timer' + index).html(html);
            });
        };

        self.endWatch = function(index) {
        	var sw = self.stopWatches[index];
            sw.stopTimer();
 			self.updateLastSplitView(index, sw.splits);
 			 var html = self.getDateHtml(sw.splits[sw.splits.length-1].formatted);
                $('#timer' + index).html(html);
	        self.bindDownloadlink(index);
        };

        self.resetWatch = function(index) {
            self.stopWatches[index].resetTimer();
             $('#splits' + index + '>tbody').html('');
        };

        self.splitWatch = function(index) {
            self.stopWatches[index].splitTimer();
            self.updateLastSplitView(index, self.stopWatches[index].splits);
        };

        self.getElapsedMs = function(index) {
            return self.stopWatches[index].time;
        };

        self.updateLastSplitView = function(index, splits) {
            var i = splits.length - 1;
            var diff = 0;
            if (i != 0) {
                diff = splits[i].total - splits[i - 1].total;
            }
            var html = '<tr><td>' + (i+1) + '</td>' + '<td>' + self.getDateHtml(splits[i].formatted) + '</td>' + '<td>' + self.getDateHtml(Pendulum.getFormatedTime(diff)) + '</td>' + '</tr>';
            $('#splits' + index + '>tbody').append(html);
        };

        self.bindDownloadlink = function(index){
			var watch = self.stopWatches[index];
			var downloadObj = {};
			downloadObj.unit = "milliseconds(ms)";
			downloadObj.splits = watch.splits.map(function(split){
				return split.total;
			})
			var fileData = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(downloadObj));
            var link = $('#export' + index );
            link.attr('href','data:' + fileData);
            link.attr('download','stopwatch'+index+'.json');
            
        };
        self.getDateHtml = function(data) {
            return ("0" + data.hours).slice(-2) + ":" + ("0" + data.minutes).slice(-2) + ":" + ("0" + data.seconds).slice(-2) + "." + ("00" + data.milliseconds).slice(-3);
        };

    });