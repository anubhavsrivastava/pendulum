(function() {

    var frameRate = 1000 / 60;
    var Stopwatch = {
        startTime: null,
        _id: 0,
        splits: [],
        isStopped: false,
        isStarted: false,
        _currentTime: function() {
            if (this.startTime) {
                var endTime = new Date();
                return (endTime - this.startTime);
            }
            return 0;
        },
        initialize: function() {
            if (this.isStarted || this.isStopped) {
                return;
            };
            this._id =  Math.floor((Math.random() * 100) + 1) + new Date().getTime();
            this.startTime = new Date();
            this.isStarted = true;
            this.isStopped = false;
            this.splits = [];
            this._timerId = setInterval(this.dispatchEvent.bind(this), frameRate);
        },
        addEventListener: function(callbackFn) {
            this.listeners = this.listeners ? this.listeners : [];
            this.listeners.push(callbackFn);
        },
        dispatchEvent: function() {
            if (this.listeners) {
                var lastTime_formatted = getFormatedTime(this._currentTime());
                this.listeners.forEach(function(callbackFn) {
                    callbackFn(lastTime_formatted);
                });
            }
        },
        splitTimer: function() {
            if (!this.isStarted || this.isStopped) {
                return;
            }
            var lastTime = this._currentTime()
            this.splits.push({total : lastTime , formatted : getFormatedTime(lastTime) });
        },
        stopTimer: function() {
            if (!this.isStarted || this.isStopped) {
                return;
            }
            this.splitTimer();
            clearInterval(this._timerId);
            this.isStopped = true;
        },
        resetTimer: function() {
            if (!this.isStopped) {
                this.stopTimer()
            };
            this.startTime = null;
            this.isStarted = false; 
            this.isStopped = false;
            this.dispatchEvent()
        },


    };

    var getFormatedTime = function(value) {
        var date = {
            hours: 0,
            minutes: 0,
            seconds: 0,
            milliseconds: 0,
        };
        if (!value) {
            return date;
        }
        date.milliseconds = Math.floor(value % 1000);
        value = value / 1000;
        date.seconds = Math.floor(value % 60);
        value = value / 60;
        date.minutes = Math.floor(value % 60);
        value = value / 60;
        date.hours = Math.floor(value % 24);
        return date;
    };

    window.Pendulum = window.Pendulum || {};
    window.Pendulum.Stopwatch = Stopwatch;
    window.Pendulum.getFormatedTime = getFormatedTime;
})();